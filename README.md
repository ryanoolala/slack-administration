# slack-administration

Script to extract all Slack users into a CSV file. This is used to find all full members of the workspace for the Slack admins for their administrative purposes

# Usage

1. Retrieve a legacy token from slack for the workspace (https://api.slack.com/custom-integrations/legacy-tokens)[https://api.slack.com/custom-integrations/legacy-tokens]
2. Paste the token into the `token` variable in slack.js
3. `npm start`

CSVs will be generated in the directory
