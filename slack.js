const fetch = require("node-fetch");
const flatten = require ("flat")
const token = ''
const createCsvWriter = require('csv-writer').createObjectCsvWriter;

async function postData(url = '', data = {}) {
  const response = await fetch(url, {
    method: 'POST', // *GET, POST, PUT, DELETE, etc.
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    },
    body: JSON.stringify(data) // body data type must match "Content-Type" header
  });
  return await response.json();
}

async function main() {
  try {
    const response = await postData('https://slack.com/api/users.list');
    const userList = response.members
    let result = "Name,email,real_name,is_admin,is_owner,is_restricted,is_ultra_restricted,is_bot\n"
    let fullMembers = [];
    let allUsers = [];
    for (const user of userList) {
      if (!user.deleted === true) {
        flattenUser = flatten(user)
        if (!user.is_admin && !user.is_owner && !user.is_restricted && !user.is_ultra_restricted && !user.is_bot) {
          fullMembers.push(flattenUser)
        }
        allUsers.push(flattenUser)
      }
    }
    await createCSV("FullMembers").writeRecords(fullMembers)
    await createCSV("AllUsers").writeRecords(allUsers)
  } catch (error) {
    console.error(error);
  }
}


function createCSV(name){
  return createCsvWriter({
    path: `${name}.csv`,
    header: [
      {id: 'name', title: 'Name'},
      {id: 'profile.email', title: 'Email'},
      {id: 'real_name', title: 'Real Name'},
      {id: 'is_admin', title: 'is_admin'},
      {id: 'is_owner', title: 'is_owner'},
      {id: 'is_restricted', title: 'is_restricted'},
      {id: 'is_ultra_restricted', title: 'is_ultra_restricted'},
      {id: 'is_bot', title: 'is_bot'},
    ]
  });
}

main()